package es.sharifylabs.sharify.time.data

import es.sharifylabs.sharify.time.model.Vehicle
import io.reactivex.Single

/** <!-- Documentation for: es.sharifylabs.sharify.time.data.StarWarsVehicleSource on 20/09/18 -->
 *
 * @author Aran Moncusí Ramírez
 */
interface StarWarsVehicleSource: BaseSource
{
    //~ Constants ======================================================================================================

    //~ Values =========================================================================================================

    //~ Properties =====================================================================================================

    //~ Methods ========================================================================================================

    fun getAllVehicles(): Single<List<Vehicle>>

    //~ Operators ======================================================================================================
}
