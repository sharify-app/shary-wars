package es.sharifylabs.sharify.time

import android.app.Application

/** <!-- Documentation for: es.sharifylabs.sharify.time.Application on 20/09/18 -->
 *
 * @author Aran Moncusí Ramírez
 */
class Application: Application()
