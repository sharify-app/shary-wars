# Sharify Wars #

API docs: `https://swapi.co/documentation#vehicles`

The goal of this app is to get the data from this api provided above, and show the data-model in a custom recycler view
created by you. We provided you the model of the object retrieved by the api.

Also, it would be nice if you could use the keasy-preferences object that is included in the project, just to see that
you can understand some custom code and you can apply it to your app.

We recommend you to use Retrofit and RxJava, we provided you the api interfaces, the reactive calls should be very
simple with a Single object.

The project could be made with any architecture that you like and we allow you to do any freedom on it.


### Final goals:
Show data in an runnable app inside a recycler view provided by the api.

When features is done, make a push request!
