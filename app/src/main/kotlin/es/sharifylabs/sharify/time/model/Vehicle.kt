package es.sharifylabs.sharify.time.model

import com.fasterxml.jackson.annotation.JsonProperty

/** <!-- Documentation for: es.sharifylabs.sharify.time.model.Vehicle on 20/09/18 -->
 *
 * *IMPORTANT!*
 * Use Jackson serialization library for work with Kotlin data classes!
 * (It been already imported in this project)
 *
 * @author Aran Moncusí Ramírez
 */
data class Vehicle(
        val name: String,

        val model: String,

        @param:JsonProperty("cost_in_credits")
        val cost: Int
)
