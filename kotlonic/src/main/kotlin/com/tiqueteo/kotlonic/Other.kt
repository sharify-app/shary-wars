/** <!-- Documentation for: com.tiqueteo.multidevice.verifier.utils.Other on 16/05/2017 -->
 *
 * @author Aran Moncusí Ramírez
 */
package com.tiqueteo.kotlonic


//~ Constants ==========================================================================================================

//~ Functions ==========================================================================================================

//~ Extensions =========================================================================================================

inline fun <T, R> T.each(init: (T) -> R?, predicate: (T, R?) -> Boolean, body: (T, R?) -> R?): R?
{
	var i: R? = init(this)

	while (predicate(this, i))
		i = body(this, i)

	return i
}

inline fun <T, E> T.eachWith(col: Iterable<E>, fn: T.(E) -> Unit): T
{
	for (value in col) this.fn(value)
	return this
}

inline fun <reified C: Any> Any.castTo(@Suppress("UNUSED_PARAMETER") ignore: kotlin.reflect.KClass<C>): C? = this as? C

inline fun <reified C: Any> Any.castTo(): C? = this as? C

inline fun <T> T.supposing(condition: (T) -> Boolean): T? = if(condition(this)) this else null

//~ Annotations ========================================================================================================

//~ Interfaces =========================================================================================================

//~ Enums ==============================================================================================================

//~ Data Classes =======================================================================================================

//~ Classes ============================================================================================================

//~ Sealed Classes =====================================================================================================

//~ Objects ============================================================================================================
